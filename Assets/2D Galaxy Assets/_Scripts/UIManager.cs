﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour {

    //Only responsbility of the UI Manager is to change the display of the UI in the game
    //-Update Display
    //manager clases DO NOT COMUNICATE WITH OTHRE SCRIPTS
    //RECEIVE MESSAGE


    public Sprite[] lives;
    //reverence for the image component
    public Image liveImageDisplay;
    public Text scoreText, bestText;
    public int score=0, bestScore;

    public GameObject titleScreen;

    private void Start()
    {
        bestScore = PlayerPrefs.GetInt("HighScore", 0);
        bestText.text = "Best: " + bestScore;
    }

    public void UpdateLifes(int currentLives)
    {

        liveImageDisplay.sprite = lives[currentLives];
        Debug.Log("PlayerLive" + currentLives);

    }

   public void UpdateScore()
    {
        score += 10;
        scoreText.text = "Score: " + score;
    }

    public void CheckForBestScore()
    {
        if(score > bestScore)
        {
            bestScore = score;
            PlayerPrefs.SetInt("HighScore", bestScore);
            bestText.text = "Best: " + bestScore;
        }
    }

   public void ShowTitleScreen()
    {
        titleScreen.SetActive(true);
        score = 0;        
    }

   public void HidetitleScreen()
    {
        titleScreen.SetActive(false);
        scoreText.text = "Score: ";
    }
    public void ResumePlay()
    {
        GameManager gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        gm.ResumeGame();
        

    }

    public void BackToMainMenu()
    {
        SceneManager.LoadScene("Main_Menu");

    }

    
}
