﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour {

    [SerializeField]
    private float _speedPowerUp = 2.5f;

    [SerializeField]
    private int powerupID; //0 TripleShot, 1 Boost Speed, 2 Shield

    //sound clip
    [SerializeField]
    private AudioClip _audioClip;

    
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        transform.Translate(Vector3.down * Time.deltaTime * _speedPowerUp);

        if(transform.position.y < -6.30f)
        {
            Destroy(this.gameObject);
        }
		
	}

    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Colide with: " + other.name);
                
        if(other.tag == "Player")
        {
            Player player = other.GetComponent<Player>();
            AudioSource.PlayClipAtPoint(_audioClip, Camera.main.transform.position);
            if (player!= null){

                if( powerupID == 0)
                {
                    player.TripleShotPowerUpOn();
                }
                else if(powerupID == 1)
                {

                    player.SpeedBoosPowerOn();

                }
                else if(powerupID == 2)
                {

                    player.EnableShields();

                }
                               

            }

            
            Destroy(this.gameObject);

        }        

        
       
    }
}
