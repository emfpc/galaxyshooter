﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class MainMenu : MonoBehaviour {

    public void LoadSinglePlayerGame()
    {
        Debug.Log("Single Player Mode");
        SceneManager.LoadScene("Single_Player");
    }
	
    public void LoadCoOpModeGame()
    {
        Debug.Log("Co-op Mode");
        SceneManager.LoadScene("Co-op_Mode");
    }
}
