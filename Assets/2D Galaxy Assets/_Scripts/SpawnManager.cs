﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour {

    //GamePrefabs for the items to spawn
    [SerializeField]
    private GameObject _enemyShipPrefab;

    [SerializeField]
    //one variable to spawn multyply items; Array
    private GameObject[] _powerUps;

    private GameManager _gameManager;

    

    

	// Use this for initialization
	void Start () {

        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
		
	}
	
    public void StartSpawnRoutines()
    {

        //to acces a Array you need to use the subset index
        //Example: _powerUps[0] this will spawn item in the 0 idex "Triple Shoot"
        StartCoroutine(EnemySpawnRoutine());
        StartCoroutine(PowerUpSpawnRoutine());

    }
    //Spawn Enemy every 5 sec
	public IEnumerator EnemySpawnRoutine()
    {
        float _randomPositionX = Random.Range(8.01f, -8.01f);

        while (_gameManager.gameOver == false)
        {
            Instantiate(_enemyShipPrefab, new Vector3(_randomPositionX, 6.35f, 0), Quaternion.identity);
            yield return new WaitForSeconds(5.0f);
        }
        
    }

    public IEnumerator PowerUpSpawnRoutine()
    {
       
        while (_gameManager.gameOver == false)
        {
            int randomPowerUp = Random.Range(0, 3);
            float _randomPositionX = Random.Range(8.01f, -8.01f);
            Instantiate(_powerUps[randomPowerUp], new Vector3(_randomPositionX, 6.35f, 0), Quaternion.identity);
            yield return new WaitForSeconds(5.0f);
        }
    }
}
