﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class Player : MonoBehaviour {

    //Players Life
    [SerializeField]
    private int _live = 3;
    //Speed of the player
    [SerializeField]
    private float _speed = 5f;

    //Laser prefab for instantiate
    [SerializeField]
    private GameObject _laserPrefab;
    [SerializeField]
    private GameObject _shieldGameObject;

    //Shooting rate
    private float _fireRate=0.25f;
    private float _canFre = 0.0f;

    //Power Ups
    public bool _canTripleShoot=false;
    public bool _canSpeedBoost = false;
    public bool _shieldsOn = false;

    //Death Explosion of the player
    [SerializeField]
    private GameObject _playerExplosion;

    //Handle for UI Manager
    private UIManager _uiManager;
    private GameManager _gameManager;
    private SpawnManager _spawnManager;

    //Reference to audio
    private AudioSource _audioSource;
    
    //Bool for Co-op Mode
    public bool isPlayer1 = false;
    public bool isPlayer2 = false;
	
	void Start () {

                
        _uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();

        if(_uiManager != null){
            _uiManager.UpdateLifes(_live);
        }
        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        if (_gameManager.IsCoOpMode == false)
        {
            transform.position = new Vector3(0, 0, 0);
        }

        _spawnManager = GameObject.Find("SpawnManager").GetComponent<SpawnManager>();

       /* if(_spawnManager != null)
        {
            _spawnManager.StartSpawnRoutines();
        }*/

        _audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {

        if(isPlayer1 == true)
        {
            Movement();
#if UNITY_ANDROID

            if ((Input.GetKeyDown(KeyCode.Space) || CrossPlatformInputManager.GetButtonDown("Fire")) && isPlayer1 == true)
            {
                Shoot();
            }
#else

if ((Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0)) && isPlayer1 == true)
            {
                Shoot();
            }
#endif

        }
        if (isPlayer2 == true)
        {
            Player2Movement();
            if (Input.GetKeyDown(KeyCode.KeypadEnter))
            {
                Shoot();
            }
            
        }   
	}

    void Movement()
    {
        float hInput = Input.GetAxis("Horizontal"); //Input.GetAxis("Horizontal");
        float vInput = Input.GetAxis("Vertical"); //Input.GetAxis("Vertical");

        if(_canSpeedBoost == true)
        {
            transform.Translate(Vector3.right * Time.deltaTime * hInput * _speed * 2.0f);
            transform.Translate(Vector3.up * Time.deltaTime * vInput * _speed * 2.0f);
        }
        else if(_canSpeedBoost == false)
        {
            transform.Translate(Vector3.right * Time.deltaTime * hInput * _speed);
            transform.Translate(Vector3.up * Time.deltaTime * vInput * _speed);
        }
        

        //position Y restrain
        if (transform.position.y > 0)
        {
            transform.position = new Vector3(transform.position.x, 0, 0);
        }
        else if (transform.position.y < -4.44f)
        {
            transform.position = new Vector3(transform.position.x, -4.44f, 0);
        }
        //position X restrain
        if (transform.position.x > 8.44)
        {
            transform.position = new Vector3(8.44f, transform.position.y, 0);
        }
        else if (transform.position.x < -8.44)
        {
            transform.position = new Vector3(-8.44f, transform.position.y, 0);
        }
    }

    void Player2Movement()
    {
        if (_canSpeedBoost == true)
        {
            if (Input.GetKey(KeyCode.Keypad8))
            {
                transform.Translate(Vector3.up * _speed * 1.5f*Time.deltaTime);
            }

            if (Input.GetKey(KeyCode.Keypad6))
            {
                transform.Translate(Vector3.right * _speed * 1.5f*Time.deltaTime);
            }

            if (Input.GetKey(KeyCode.Keypad2))
            {
                transform.Translate(Vector3.down * _speed * 1.5f * Time.deltaTime);
            }

            if (Input.GetKey(KeyCode.Keypad4))
            {
                transform.Translate(Vector3.left * _speed * 1.5f*Time.deltaTime);
            }
        }
        else if (_canSpeedBoost == false)
        {
            if (Input.GetKey(KeyCode.Keypad8))
            {
                transform.Translate(Vector3.up * _speed * Time.deltaTime);
            }

            if (Input.GetKey(KeyCode.Keypad6))
            {
                transform.Translate(Vector3.right * _speed * Time.deltaTime);
            }

            if (Input.GetKey(KeyCode.Keypad2))
            {
                transform.Translate(Vector3.down * _speed * Time.deltaTime);
            }

            if (Input.GetKey(KeyCode.Keypad4))
            {
                transform.Translate(Vector3.left * _speed * Time.deltaTime);
            }
        }


        //position Y restrain
        if (transform.position.y > 0)
        {
            transform.position = new Vector3(transform.position.x, 0, 0);
        }
        else if (transform.position.y < -4.44f)
        {
            transform.position = new Vector3(transform.position.x, -4.44f, 0);
        }
        //position X restrain
        if (transform.position.x > 8.44)
        {
            transform.position = new Vector3(8.44f, transform.position.y, 0);
        }
        else if (transform.position.x < -8.44)
        {
            transform.position = new Vector3(-8.44f, transform.position.y, 0);
        }
    }

    public void Damage()
    {
        if(_shieldsOn == true)
        {

            _shieldsOn = false;
            _shieldGameObject.SetActive(false);
            //stop the program
            return;


        }
        else if (_shieldsOn == false)
        {
            _live--;
            _uiManager.UpdateLifes(_live);

            if (_live < 1)
            {
                Instantiate(_playerExplosion, transform.position, Quaternion.identity);
                _gameManager.gameOver = true;
                _uiManager.CheckForBestScore();
                _uiManager.ShowTitleScreen();                
                Destroy(this.gameObject);
            }
        }
    }

    void Shoot()
    {
        //The logic for a coold down system / codes to run in a givin time
        //the power up must comunicate to the player script to run the "TripleShotPowerUpOn"
        //since it the destory the power up item
        if (Time.time > _canFre)
        {
            _audioSource.Play();
            if(_canTripleShoot == true)
            {
                Instantiate(_laserPrefab, transform.position + new Vector3(0, 0.85f, 0), Quaternion.identity);
                Instantiate(_laserPrefab, transform.position + new Vector3(0.41f, 0.21f, 0), Quaternion.identity);
                Instantiate(_laserPrefab, transform.position + new Vector3(-0.41f, 0.21f, 0), Quaternion.identity);
            }
            else
            {
                Instantiate(_laserPrefab, transform.position + new Vector3(0, 0.85f, 0), Quaternion.identity);
               
            }
            _canFre = Time.deltaTime + _fireRate;

        }
        

        
    }

    

    public void TripleShotPowerUpOn()
    {
        _canTripleShoot = true;
        StartCoroutine(TripleShotPowerDownRoutine());
    }

    public void SpeedBoosPowerOn()
    {
        _canSpeedBoost = true;
        StartCoroutine(SpeedBoostPowerDownRoutine());
    }

    public void EnableShields()
    {
        _shieldsOn = true;
        _shieldGameObject.SetActive(true);
    }
    public IEnumerator TripleShotPowerDownRoutine()
    {
        yield return new WaitForSeconds(5.0f);
        _canTripleShoot = false;
    }

    public IEnumerator SpeedBoostPowerDownRoutine()
    {
        yield return new WaitForSeconds(5.0f);
        _canSpeedBoost = false;
    }
}
