﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour {
    [SerializeField]
    private float _enemySpeed = 0.5f;

    [SerializeField]
    private GameObject _enemyExplosion;

    private UIManager _uiManager;

    //Reference audio for the clip
    //Audioclip is a way to instantiate a sound in any position; if you use audiosouce and the GameObject is destroy the sound
    //won't run
    [SerializeField]
    private AudioClip _audioClip;

    //variables for random value in the x and y
   

	// Use this for initialization
	void Start () {

        _uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();

       
	}
	
	// Update is called once per frame
	void Update () {

        transform.Translate(Vector3.down * Time.deltaTime * _enemySpeed);
        
        if(transform.position.y < -6.4)
        {
            float _randomX = Random.Range(7.85f, -7.85f);
            transform.position = new Vector3(_randomX, 6.40f, 0);
        }
		
	}

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Laser")
        {
            Destroy(other.gameObject);
            Instantiate(_enemyExplosion, transform.position, Quaternion.identity);
            _uiManager.UpdateScore();
            AudioSource.PlayClipAtPoint(_audioClip, Camera.main.transform.position);
            Destroy(this.gameObject);
            
        }
        else if(other.tag == "Player")
        {
            Player player = other.GetComponent<Player>();
            if(player != null)
            {
                player.Damage();
            }

            Instantiate(_enemyExplosion, transform.position, Quaternion.identity);
           // _uiManager.UpdateScore();
            AudioSource.PlayClipAtPoint(_audioClip, Camera.main.transform.position);
            Destroy(this.gameObject);
            
        }


    }
}
