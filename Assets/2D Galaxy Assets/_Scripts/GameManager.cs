﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public bool IsCoOpMode = false;

    //bool to know if the game in on
    public bool gameOver = true;
    [SerializeField]
    private GameObject _player;
    [SerializeField]
    private GameObject _coopPlayers;
    private UIManager _uiManager;
    private SpawnManager _spawnManager;

    [SerializeField]
    private GameObject _pauseMenuPanel;

    //Fetch Animator
    private Animator _pauseAnimator;


    private void Start()
    {
        _uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();
        _spawnManager = GameObject.Find("SpawnManager").GetComponent<SpawnManager>();

        if(IsCoOpMode == false)
        {
            _pauseAnimator = GameObject.Find("Pause_Menu_Panel").GetComponent<Animator>();
            _pauseAnimator.updateMode = AnimatorUpdateMode.UnscaledTime;
        }
       
    }

    private void Update()
    {
        if (gameOver == true)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if(IsCoOpMode == false)
                {
                    //Vector3.zero is the same as new Vector3 (0,0,0)
                    Instantiate(_player, Vector3.zero, Quaternion.identity);
                }
                else if (IsCoOpMode == true)
                {
                    Instantiate(_coopPlayers, Vector3.zero, Quaternion.identity);
                }
                
                gameOver = false;
                _uiManager.HidetitleScreen();
                _spawnManager.StartSpawnRoutines();
            }
            else if (Input.GetKeyDown(KeyCode.Escape))
            {
                SceneManager.LoadScene("Main_Menu");
            }
        }

        if (Input.GetKeyDown(KeyCode.P))
        {            
            _pauseMenuPanel.SetActive(true);
            _pauseAnimator.SetBool("isPaused", true);
            Time.timeScale = 0;
        }

    }

    public void ResumeGame()
    {
        _pauseMenuPanel.SetActive(false);
        Time.timeScale = 1;
    }

    
}
